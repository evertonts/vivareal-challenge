# Challenge

API REST para criar, mostrar e buscar imóveis em Spotippos

Setup
=======================
Para preparar o projeto basta rodar o script de setup

```bash
bin/setup
```

Para rodar os testes:

```
bin/rspec
```

Rodar a aplicação:

```bash
bin/rails s
```

Dependências
============

- Ruby 2.4
- PostgreSQL
